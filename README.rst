===============================
horizon-mellanox
===============================

OpenStack Boilerplate contains all the boilerplate you need to create an OpenStack package.

Please feel here a long description which must be at least 3 lines wrapped on
80 cols, so that distribution package maintainers can use it in their packages.
Note that this is a hard requirement.

* Free software: Apache license
* Documentation: http://docs.openstack.org/developer/horizon-mellanox
* Source: http://git.openstack.org/cgit/horizon-mellanox/horizon-mellanox
* Bugs: http://bugs.launchpad.net/horizon-mellanox

Features
--------

* TODO
