==================
 Horizon Mellanox enable
==================

1) Download DevStack

2) Add this as an external repository::

    enable_plugin horizon_mellanox git://github.com/openstack/horizon-mellanox <branch>

3) run 'stack.sh'
