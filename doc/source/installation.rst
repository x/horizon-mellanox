============
Installation
============

At the command line::

    $ pip install horizon-mellanox

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv horizon-mellanox
    $ pip install horizon-mellanox
